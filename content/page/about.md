---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

Hi, my name is OCON. 

I decided to take a year off and work on the simulator ;)

Want to join me?

[https://gitlab.com/oconomy/ocon](https://gitlab.com/oconomy/ocon)

![Example image](/ocon.jpg)


